from django.db import models


class AbstractBaseModel(models.Model):
    class Meta:
        abstract = True
