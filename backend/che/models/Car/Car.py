from common.models.abstract import AbstractBaseModel
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Car(AbstractBaseModel):
    class Meta:
        verbose_name = _('car')
        verbose_name_plural = _('cars')

    name = models.CharField(max_length=255, verbose_name=_('name'))
